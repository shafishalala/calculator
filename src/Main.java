import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the first number: ");
        double num1 = scanner.nextDouble();

        System.out.print("Enter the second number: ");
        double num2 = scanner.nextDouble();

        System.out.println("Select an operation:");
        System.out.println("1. Addition");
        System.out.println("2. Subtraction");
        System.out.println("3. Multiplication");
        System.out.println("4. Division");

        int choice = scanner.nextInt();

        double result = 0.0;
        String operation = "";

        switch (choice) {
            case 1:
                operators operations;
                result = operators.addition(num1, num2);
                operation = "Addition";
                break;
            case 2:
                result = operators.subtraction(num1, num2);
                operation = "Subtraction";
                break;
            case 3:
                result = operators.multiplication(num1, num2);
                operation = "Multiplication";
                break;
            case 4:
                result = operators.division(num1, num2);
                operation = "Division";
                break;
            default:
                System.out.println("Invalid choice!");
                return;
        }

        System.out.println(operation + " Result: " + result);
    }
}